(* Executable wrapper around the z80disasm libarry
*)

let app_name = "z80disasm"
let app_version = "0.1"
let usage_msg = "-i instruction count -o offset [-v]"
let instr_count = ref 15
let offset = ref 0
let input_files = ref []

let anon_fun filename = input_files := filename :: !input_files
let speclist =
  [
    ("-i", Arg.Set_int instr_count, "Number of iterations");
    ("-o", Arg.Set_int offset, "Start offset");
    ("-v", Arg.Unit (fun () -> Core.printf "%s %s\n" app_name app_version; exit 0), "Display version" );
  ]


let () =
  let _ = Arg.parse speclist anon_fun usage_msg in
  let input = if List.length !input_files == 0 then "samples/zmonos.bin" else (Twutils.head !input_files) in
  let ic = Core.In_channel.create ~binary:true  input in (* "samples/zmonos.bin" *)
  let count = match Core.Int.of_int64 (In_channel.length ic) with
    | None -> 0
    | Some l -> l
  in
  let buffer = Buffer.create count in
  let _ = Buffer.add_channel buffer ic count in
  let _ = close_in ic in
  let runcfg = Z80disasm.create_run_config !instr_count !offset in
  Z80disasm.disasm runcfg buffer
