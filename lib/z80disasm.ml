
type run_config =
  {
    instruction_count: int;
    offset: int;                (* Starting offset into the buffer *)
    (* base_addr: int; *)
    (* current_addr: int; *)
  }

let create_run_config ic offset =
  {
    instruction_count = ic;
    offset = offset;
    (* base_addr = 0x0000; *)
    (* current_addr = 0; *)
  }

type op_flags =
  {
    ix  : bool;
    iy  : bool;
    bit : bool;
}

let create_start_flags = { ix=false; iy=false; bit=false }

(* https://clrhome.org/table/#%20 *)

type opcode = {
  start_opcode : int;           (* Non family start == end *)
  end_opcode : int;
  nibble : int;
  display : string;
  op_bytes : int;               (* number of following bytes the opcode operates
                                   on *)
  family : bool;                (* Operates on registers *)
  bits : int;                   (* 0, 8 or 16 bits *)
}

let opcodes =
  [
    { start_opcode=0x00; end_opcode=0x00; nibble=0x00; display="nop"; op_bytes=0; family=false; bits=0 };
    { start_opcode=0x01; end_opcode=0x31; nibble=0x01; display="ld"; op_bytes=2; family=true; bits=16 };
    { start_opcode=0x03; end_opcode=0x33; nibble=0x03; display="inc"; op_bytes=0; family=true; bits=16 };
    { start_opcode=0x04; end_opcode=0x34; nibble=0x04; display="inc"; op_bytes=0; family=true; bits=8 };
    { start_opcode=0x0c; end_opcode=0x3c; nibble=0x0c; display="inc"; op_bytes=0; family=true; bits=9 };
    { start_opcode=0x4e; end_opcode=0x7e; nibble=0x0e; display="ld\t%R, (hl)"; op_bytes=0; family=true; bits=8 };
    { start_opcode=0xc3; end_opcode=0xc3; nibble=0x00; display="jp"; op_bytes=2; family=false; bits=0 };
    { start_opcode=0xc9; end_opcode=0xc9; nibble=0x00; display="ret"; op_bytes=0; family=false; bits=0 };
    { start_opcode=0xcd; end_opcode=0xcd; nibble=0x00; display="call"; op_bytes=2; family=false; bits=0 };
    { start_opcode=0xf3; end_opcode=0xf3; nibble=0x00; display="di"; op_bytes= 0; family=false; bits=0 };
  ]

let handle_explicit_opcode buf cur opc =
  let org_cur = cur in
  let out1 = Core.sprintf "%s" opc.display in
  let (cur', out2) = if opc.op_bytes == 0
    then (cur, "")
    else (cur+2, Disutils.get_hex_word buf cur (*opc.op_bytes*)) in
  (cur', opc.op_bytes+1, org_cur, out1 ^ "\t" ^ out2)

let rec flatten_op op =
  if Disutils.high_nibble op > 3 then flatten_op (op-0x10) else op

let reg16 = [ "bc"; "de"; "hl"; "sp" ]
let get_reg_16 i = List.nth reg16 i

let reg8_1 = [ "b"; "d"; "h" ]
let reg8_2 = [ "c"; "e"; "l"; "a" ]

let get_reg_8_1 i = List.nth reg8_1 i
let get_reg_8_2 i = List.nth reg8_2 i

let get_reg flags i opc =
  if flags.ix == true
  then
    let _ = Core.printf "REG:%d\n" i in
    match opc.bits with
    | 16 -> "ix"
    | 9  -> get_reg_8_2 i
    | 8  -> get_reg_8_1 i
    | _  -> Core.sprintf "UNKREG:%d" i
  else
    match opc.bits with
    | 16 -> get_reg_16 i
    | 9  -> get_reg_8_2 i
    | 8  -> get_reg_8_1 i
    | _  -> Core.sprintf "UNKREG:%d" i

let handle_family_opcode opcode buf cur opc flags =
  let flattened = flatten_op opcode in
  (* let _ = Core.printf "OP: %02x %02x %02x\n" opcode flattened (Disutils.high_nibble flattened) in *)
  let register = if opc.family then get_reg flags (Disutils.high_nibble flattened) opc else "" in
  let value = if opc.op_bytes > 0 then ", $" ^ Disutils.get_hex_bytes buf cur opc.op_bytes else "" in
  (cur+opc.op_bytes, opc.op_bytes+1, Core.sprintf "%s\t%s%s" opc.display register value)

(* let check_ix op buf cur = *)
(* let check_ix  op _  cur = *)
(*   let (f, cur') = if op == 0xdd then *)
(*       (true, cur+1) *)
(*     else *)
(*       (false, cur) in *)
(*   (f, cur') *)

(*
   IX register (0xdd)
   IY register (0xfd)

   Set the IX/IY flag
   Read the next opcode
   Handle as usual
*)

let get_extra_count f =
  if f.ix || f.iy then 1
  else 0

let rec handle_opcode (op : int)
    (buf : Buffer.t)
    (cur_ptr : int)
    (flags : op_flags) =
  let orig_cur = cur_ptr - 1 in (* store this for the hex dump *)
  if op == 0xdd                 (* IX *)
  then
    (* get nextop code and call back in *)
    let (op', cur') = Disutils.get_int buf cur_ptr in
    handle_opcode op' buf cur' {flags with ix = true}
  else
    let details' = List.find_opt
        (fun opc ->
           if opc.family == false &&
              opc.start_opcode == op
           then true
           else false)
        opcodes in
    let (cur', final_count, final_orig, output) = match details' with
      | Some details -> handle_explicit_opcode buf cur_ptr details
      | None -> let details' = List.find_opt
                    (fun opc ->
                       if opc.family == true &&
                          op >= opc.start_opcode && op <= opc.end_opcode &&
                          Disutils.low_nibble op == opc.nibble
                       then true
                       else false)
                    opcodes in
        let (cur'', count, output') = match details' with
          | Some details -> handle_family_opcode op buf cur_ptr details flags
          | None         -> (cur_ptr, 1, Core.sprintf ";; UNKNOWN: 0x%02x" op) in
        let (orig', count') = (orig_cur - (get_extra_count flags), count + (get_extra_count flags)) in
        (cur'', count', orig', output') in

    Core.printf "%04x %s \t%s\n"
      final_orig
      (Disutils.get_hex_bytes buf final_orig final_count)
      output;
    (cur')

let rec disasm' runcfg inp cur len =
  match runcfg with
  | { instruction_count = ic; _ } ->
    if ic == 0 then
      ()
    else
      match cur with
      | l when l == len -> ()
      | _ -> let (op1, cur') = Disutils.get_char inp cur in
        let cur'' = handle_opcode (Char.code op1) inp cur' create_start_flags in
        let runcfg' = { runcfg with instruction_count = runcfg.instruction_count - 1 } in
        disasm' runcfg' inp cur'' len

let disasm runcfg inp =
  let len = Buffer.length inp in
  disasm' runcfg inp (runcfg.offset) len
