
let int_or_hex_to_int s =
  let s' = Core.String.lowercase s in
  int_of_string s'
