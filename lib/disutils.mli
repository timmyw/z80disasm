
(** Retrieve the specified byte as a char.  Increments the offset and returns a
    tuple of the character and offset *)
val get_char : Buffer.t -> int -> (char * int)

(** Retrieve specifed byte as int.  Increments offset as per get_char *)
val get_int : Buffer.t -> int -> (int * int)

(** Retrieve specified number of bytes as a hex string *)
val get_hex_bytes : Buffer.t -> int -> int -> string

(** Retrieve a hex word (2 bytes) string.  Note that these are switched around
    to match z80 endianness *)
val get_hex_word : Buffer.t -> int -> string

(** Retrieve the high nibble of the supplied int *)
val high_nibble : int -> int

(** Retrieve the low nibble of the supplied int *)
val low_nibble : int -> int
