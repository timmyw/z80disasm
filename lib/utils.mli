
(** Convert the supplied string from hexadecimal (prefix of '0x' or '0X') to
    int, or treats the string as an int *)
val int_or_hex_to_int : string -> int
