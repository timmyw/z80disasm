
let get_char b c =
  (Buffer.nth b c, c+1)

let get_int b c =
  let (ch, c') = get_char b c in
  (Char.code ch, c')

let get_hex_word buf cur =
  let (b_hi, cur') = get_char buf cur in
  let (b_lo, _) = get_char buf cur' in
  Core.sprintf "%04x" (Char.code b_hi + Int.shift_left (Char.code b_lo) 8)

let rec get_hex_bytes' buf cur cnt acc =
  match cnt with
  | 0 -> acc
  | _ ->
    let (ch, cur') = get_char buf cur in
    let acc' = acc ^ Core.sprintf "%02x" (Char.code ch) in
    get_hex_bytes' buf cur' (cnt-1) acc'

let get_hex_bytes buf cur cnt =
  get_hex_bytes' buf cur cnt ""

  (* match cnt with *)
  (* | 2 -> get_hex_word buf cur (\* Separate to switch for endianness *\) *)
  (* | _ -> get_hex_bytes' buf cur cnt "" *)

let high_nibble x = (x land 0xf0) lsr 4

let low_nibble x = (x land 0x0f)
