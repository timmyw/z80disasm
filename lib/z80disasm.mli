
type run_config

(** [create_run_config c o] will create a run time config with [c] instruction count and [o] start offset*)
val create_run_config : int -> int -> run_config

(** Dissassemble the supplied {!Buffer} *)
val disasm : run_config -> Buffer.t -> unit
